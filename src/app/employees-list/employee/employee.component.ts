import { Component, OnInit } from '@angular/core';
import { GridColumnHeaders } from '../grid/model/grid-column-headers';
import { MatDialog } from '@angular/material/dialog';
import { AddEmployeeComponent } from '../components/add-employee/add-employee.component';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  gridColumnHeaders: GridColumnHeaders[];
  staticData: [] = [];

  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
   
    this.gridColumnHeaders = [ 
      new GridColumnHeaders( {
        label: "Employee Name", id: 'employeeName'
      }), 
      new GridColumnHeaders( {
        label: "Designation", id: 'designation'
      }),
      new GridColumnHeaders( {
        label: "In Time", id: 'in_time', type: 'date'
      }),
      new GridColumnHeaders( {
        label: "Out Time", id: 'out_time', type: 'date'
      }),
      new GridColumnHeaders( {
        label: "Duration", id: 'duration'
      })      
    ];
    if(localStorage.getItem("empObj")){
      this.staticData = JSON.parse(localStorage.getItem("empObj"));
    }
  }

  addEmployee(){

    const dialogRef = this.dialog.open(AddEmployeeComponent, {
      width: '500px',
      data: []
    });

    dialogRef.afterClosed().subscribe(result => {
      this.staticData = JSON.parse(localStorage.getItem("empObj"));
      console.log('The dialog was closed');
    }); 
  }

}
