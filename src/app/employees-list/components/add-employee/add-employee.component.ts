import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  detailsForm: FormGroup;

  constructor(public formBuilder: FormBuilder, private dialog: MatDialog,
    public dialogRef: MatDialogRef<AddEmployeeComponent>,) { 
    this.initializeForm();
  }

  initializeForm(){
    this.detailsForm = this.formBuilder.group({
      fName: ['', []],
      lName: ['', []],
      email: ['', []],
      designation: ['', []],
    });
  }
  
  ngOnInit(): void {
  }

  saveEmployee(){
    let obj = { 
      employeeName: this.detailsForm.value.fName +" "+ this.detailsForm.value.lName,
      designation: this.detailsForm.value.designation
    }
    let empObj = localStorage.getItem("empObj");
    if(empObj){
      let obj1= JSON.parse(empObj);
      obj1.push(obj);
      localStorage.setItem("empObj", JSON.stringify(obj1))
    }
    else{
      let obj1= [];
      obj1.push(obj);
      localStorage.setItem("empObj", JSON.stringify(obj1))
    }
    this.dialogRef.close();
  }

  cancel(){
    this.dialogRef.close();
  }
}
