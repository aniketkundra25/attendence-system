import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';


const homeRoutes: Routes = [
    {
        path: '', 
        component : EmployeeComponent
    }  
];

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
})

export class EmployeesRoutingModule { }