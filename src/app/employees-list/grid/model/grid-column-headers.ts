export class GridColumnHeaders {
    label: string;
    id: string;
    isSortingDisabled: boolean;
    type : string;
    route : string;

    constructor(
        options: {
            label?: string,
            id?: string,
            isSortingDisabled?: boolean,
            type?:string,
            route? : string
        } = {}
    ) {
        this.label = options.label;
        this.id = options.id;
        this.isSortingDisabled = options.isSortingDisabled;
        this.type = options.type;
        this.route = options.route;
    }
}
