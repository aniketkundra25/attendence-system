export class CustomColumnButttons{
    label:string; 
    isDisabled:boolean;
    mappingField:string;
    customColumnClkHandler:Function;

    constructor(label:string,isDisabled:boolean,mappingField:string,customColumnClkHandler:Function){
        this.label = label;
        this.isDisabled = isDisabled;
        this.mappingField = mappingField;
        this.customColumnClkHandler = customColumnClkHandler;
    }
}