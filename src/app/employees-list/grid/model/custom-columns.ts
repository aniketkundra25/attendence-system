import { CustomColumnButttons } from './custom-column-buttons';
import { CustomColumnIcons } from './custom-column-icons';
import { Component } from '@angular/core';

export class CustomColumns{
    title:string;
    type:string;
    sequence:number;
    cssClass:string;
    includeComponent:Component;
    showGridFilter:boolean =true;
    buttonsOrIcons:CustomColumnButttons[] | CustomColumnIcons[];

    constructor(options:{
        title?:string,
        type?:string,
        sequence?:number,
        cssClass?:string,
        includeComponent?:any,
        showGridFilter?:boolean,
        buttonsOrIcons?:CustomColumnButttons[] | CustomColumnIcons[],
      } = {}){
        this.title = options.title;
        //this.type = options.type;
        this.sequence = options.sequence;
        this.cssClass = options.cssClass;
        this.includeComponent = options.includeComponent;
        this.showGridFilter = options.showGridFilter;
        this.buttonsOrIcons = options.buttonsOrIcons;   
        this.type = options.type;
    }
}