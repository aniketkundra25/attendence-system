export class CustomColumnIcons{
    iconName:string; 
    isDisabled:boolean;
    tooltip:string;
    customColumnClkHandler:Function;

    constructor(iconName:string,isDisabled:boolean,tooltip:string,customColumnClkHandler:Function){
        this.iconName = iconName;
        this.isDisabled = isDisabled;
        this.tooltip = tooltip;
        this.customColumnClkHandler = customColumnClkHandler;
    }
}