import { Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { GridColumnHeaders } from './model/grid-column-headers';
import { CustomColumns } from './model/custom-columns';
import { GridService } from './service/grid.service';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit,OnChanges {

  @Input() enableRowSelection : boolean;
  @Input() restUrl;
  @Input() staticData = [];
  @Input() gridColumnHeaders: GridColumnHeaders[];
  @Input() gridColumnUrl: string;
  @Input() gridHeaderText: string;
  @Input() customColumns: CustomColumns[];
  @Input() dynamicCustomColumns: CustomColumns[];
  @Input() resultProperty: string;
  @Input() resultTotalCountProperty: string;
  @Input() firstResultProperty: string;
  @Input() fetchSizeProperty: string;
  @Input() paginationLimitOptions;
  @Input() pageSize: number;
  @Input() pageIndex: number;
  @Input() restUrlChange: boolean;
  @Input() updateGridData: boolean;
  @Input() showPaginationFirstLastButtons: boolean;
  @Input() hidePaginatorForStaticData : boolean
  @Input() gridSortingDisabled;
  @Input() sortFieldproperty : string;
  @Input() sortDirectionProperty : string;
  @Input() addBorderHeader : boolean;
  @Output() dataFromComponentHandler = new EventEmitter();
  
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  displayedColumns = [];
  resultsLength : number= 0;
  hidePaginator : boolean = false;
  sortColumn : string;
  sortOrder : string;
  pageEvent: PageEvent;
  productDetails:{[k: string]: any} = {};

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatMenuTrigger,{static:false}) productMenu: MatMenuTrigger;

  constructor(private gridService : GridService,
              private cd : ChangeDetectorRef) {

    this.enableRowSelection = false;
    this.resultProperty = "results";
    this.resultTotalCountProperty = "totalCount";
    this.firstResultProperty = "skip";
    this.fetchSizeProperty = "top";
    this.paginationLimitOptions = [5, 10, 25, 100];
    this.pageSize = 5;
    this.pageIndex = 1;
    this.enableRowSelection = false;
    this.gridColumnHeaders = [];
    this.customColumns = [];
    this.showPaginationFirstLastButtons = true;
    this.hidePaginatorForStaticData = true;
    this.gridSortingDisabled = true;
    this.sortFieldproperty = 'sortField';
    this.sortDirectionProperty = 'sortDirection';
    this.addBorderHeader = false;
   }

  ngOnInit() {

    if (this.gridColumnUrl) {
      this.makeGridHeaders();
    } else {
      this.makeTableColumns();
    }

    this.refreshGrid();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.restUrlChange != undefined && changes.restUrlChange.currentValue != undefined) {
      this.paginator.pageIndex = 0;
      this.refreshGrid();
      this.cd.detectChanges();

    }
    if (changes.staticData != undefined && changes.staticData.currentValue != undefined) {
      this.paginator.pageIndex = 0;
      this.staticData = changes.staticData.currentValue;
      this.refreshGrid();
      this.cd.detectChanges();
    }
    if (changes.updateGridData != undefined && changes.updateGridData.currentValue != undefined) {
      this.refreshGrid();
    }
    if (changes.gridUrlChange != undefined && changes.gridUrlChange.currentValue != undefined) {
      this.paginator.pageIndex = 0;
      if (this.gridColumnUrl) {
        this.makeGridHeaders();
      } else {
        this.makeTableColumns();
      }
      this.refreshGrid();
      this.cd.detectChanges();
    }
  
  }

  makeGridHeaders() {
    this.gridService.gridColumnUrl(this.gridColumnUrl).subscribe((result:any) => {
      this.gridColumnHeaders = result;
      this.makeTableColumns();
    })
  }

  makeTableColumns() {
    if (this.gridColumnHeaders && this.gridColumnHeaders.length > 0 && this.dynamicCustomColumns && this.dynamicCustomColumns.length > 0) {
      for (let i = 0; i < this.gridColumnHeaders.length; i++) {
        
        for (let j = 0; j < this.dynamicCustomColumns.length; j++) {
          if (this.dynamicCustomColumns[j].title.toLowerCase() === this.gridColumnHeaders[i].id.toLowerCase()) {
            this.gridColumnHeaders[i]['includeComponent'] = this.dynamicCustomColumns[j].includeComponent;
          }
        }
      }
    }
    this.displayedColumns = this.gridColumnHeaders.map(x => x.id);
    if (this.customColumns && this.customColumns.length > 0) {
      for (let i = 0; i < this.customColumns.length; i++) {
        if (this.customColumns[i].sequence != undefined) {
          this.displayedColumns.splice(this.customColumns[i].sequence, 0, this.customColumns[i].title);
        } else {
          this.displayedColumns.push(this.customColumns[i].title);
        }
      }
    }

    if (this.enableRowSelection) {
      this.displayedColumns.splice(0, 0, "select");
    }
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  

  refreshGrid(){
    if(this.restUrl){
      let params = {};
      params[this.fetchSizeProperty] = this.pageSize;
      params[this.firstResultProperty] = this.pageIndex;
      if(!this.gridSortingDisabled && this.sortColumn){
        params[this.sortFieldproperty] = this.sortColumn;
        params[this.sortDirectionProperty] = this.sortOrder;
      }
      this.gridService.getGridData(this.restUrl,params).subscribe((result)=>{
        this.dataSource.data = result[this.resultProperty];
        this.resultsLength = result[this.resultTotalCountProperty];
      })
    }else{
      this.resultsLength = this.staticData.length;
      this.dataSource._updatePaginator( this.resultsLength);
      if(this.hidePaginatorForStaticData){
        this.dataSource.data = this.staticData;
        this.hidePaginator = true;
        return;
      }
      let renderedData = this.staticData;
      renderedData = renderedData.slice((this.pageIndex - 1) * this.pageSize,this.pageSize + ((this.pageIndex -1) * this.pageSize));
      this.dataSource.data = renderedData;
    }
  }

  getData($event) {
    if (this.selection.selected.length > 0) {
      this.selection.clear();
    }
    this.pageIndex = $event.pageIndex + 1;
    this.pageSize = $event.pageSize;
    this.refreshGrid();
    return $event;
  }

  sortData(event) {
    if (this.selection.selected.length > 0) {
      this.selection.clear();
    }
  
    this.sortColumn = event.active;
    this.sortOrder = event.direction;

    this.pageIndex = 1;
    this.paginator.pageIndex = 0;
    this.refreshGrid();
  }

  dataFromComponent(data) {
    if (Object.keys(data).length > 0) {
      this.dataFromComponentHandler.emit(data);
    }
  }

}
